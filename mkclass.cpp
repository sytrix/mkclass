#include <iostream>
#include <string>
#include <algorithm>
#include <stdio.h>

bool fileExist(const char *filepath) {
	FILE *file = fopen(filepath, "r");

	if (file) {
		fclose(file);
		return true;
	}

	return false;
}

bool createFileWithContent(const char *filepath, const char *content) {
	FILE *file = fopen(filepath, "w");

	if (file) {
		fputs(content, file);
		fclose(file);
		return true;
	}

	return false;
}

int main(int argc, char *argv[]) {

	if (argc == 2) {
		std::string className = std::string(argv[1]);
		std::string fileSource = className + ".cpp";
		std::string fileHeader = className + ".hpp";
		bool isSourceFileExist = fileExist(fileSource.c_str());
		bool isHeaderFileExist = fileExist(fileHeader.c_str());

		if (isSourceFileExist || isHeaderFileExist) {
			std::cout << "class file exist" << std::endl;
		} else {
			std::string classNameUpper = "";
			std::transform(className.begin(), className.end(), classNameUpper.begin(), ::toupper);

			for (std::basic_string<char>::iterator p = className.begin(); p != className.end(); ++p) {
				classNameUpper += toupper(*p);
			}

			std::string fileHeaderContent = "";
			std::string fileSourceContent = "";

			fileHeaderContent += "#ifndef " + classNameUpper + "_H\n";
			fileHeaderContent += "#define " + classNameUpper + "_H\n";
			fileHeaderContent += "\n";
			fileHeaderContent += "class " + className + "\n";
			fileHeaderContent += "{\n";
			fileHeaderContent += "\tpublic:\n";
			fileHeaderContent += "\t\t" + className + "();\n";
			fileHeaderContent += "\t\t~" + className + "();\n";
			fileHeaderContent += "\t\t\n";
			fileHeaderContent += "\tprotected:\n";
			fileHeaderContent += "\t\t\n";
			fileHeaderContent += "\tprivate:\n";
			fileHeaderContent += "\t\t\n";
			fileHeaderContent += "};\n";
			fileHeaderContent += "\n";
			fileHeaderContent += "#endif";
			fileHeaderContent += "\n";

			fileSourceContent += "#include \"" + fileHeader + "\"\n";
			fileSourceContent += "\n";
			fileSourceContent += "\n";
			fileSourceContent += "\n";
			fileSourceContent += className + "::" + className + "() {\n";
			fileSourceContent += "\t\n";
			fileSourceContent += "}\n";
			fileSourceContent += "\n";
			fileSourceContent += className + "::~" + className + "() {\n";
			fileSourceContent += "\t\n";
			fileSourceContent += "}\n";
			fileSourceContent += "\n";

			createFileWithContent(fileSource.c_str(), fileSourceContent.c_str());
			createFileWithContent(fileHeader.c_str(), fileHeaderContent.c_str());

		}

	} else {
		std::cout << argv[0] << " <className>" << std::endl;
	}

	return 0;
}
